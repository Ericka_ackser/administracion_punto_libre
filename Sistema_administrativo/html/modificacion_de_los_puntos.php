<?php
session_start();

if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true){  
  
  ?>
  
  


<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Modificacion de los puntos libres</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/>   
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    
    
    <script type="text/javascript" src="../js/comunes.js"></script>
    <script type="text/javascript" src="../js/deshabilitar.js"></script>


  </head>

  <body>
      <?php
        include("cabecera.php")
      ?>                                       
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                            <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.php"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.php" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.php"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.php"><i class="icon-file"></i> Reportes</a></li>
                            <li class="active" title="Modificacion y deshabilitacion de los Puntos"><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i>  Modificar y deshabilitar P.L</a></li>
      
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>


                        <div class="span9" id="#contenido">
                        <div class="hero-unit">
                          
                          <h3 class="text-center">Modificacion de los puntos libres</h3>
            
   
                            <div class="row-fluid">
                                <div class="span12 text-center btn-primary">
                                  <span>Estados donde se encuentran los Punto libre</span>
                                </div>
                            </div>
                                    
   
                      
                     <table class="table table-striped" >
                      <tr>
                        <td>
                          Elije el estado y Dirección donde deseas modificar o Deshabilitar
                        </td>
                      </tr>
                      </table>
                     
                      <table  class="table table-striped">        
                            <tr>
				<th>Estado</th>
				<th>Direccion</th>
				<th>Codigo de Punto</th>
                                <th>Situacion actual</th>
			    </tr>

                            <?php include("../php/ubicacion_de_los_puntos.php");
                            
  
			  for($i=0;$i<count($array_salida);$i++){ ?>
                            <tr>
				<td><?php echo $array_salida[$i]["estado"];?></td>
				<td><?php echo $array_salida[$i]["direccion"];?></td>
				<td><?php echo $array_salida[$i]["idpunto_libre"];?></td>
			        <td><?php echo $array_salida[$i]["situacion_actual"]; ?></td>
				
				
			  <?php   } ?>
                          
                             <tr>
				<td colspan="2">
                                  Elige esta opcion para modificar el Punto Libre que desees. Introduciendo el codigo de Punto especificado en la tabla:
				</td> 

			
                                <td>
				<a class="btn btn-primary btn-mini" href="#myModal1" data-toggle="modal" role="button">
                                  <i class="icon-file icon-white"></i> Modificar
				</a>
                                </td>
                                 <td>
				  <a class="btn btn-mini btn-danger" href="#myModal2"  data-toggle="modal" role="button" >
				  <i class="icon-user icon-remove icon-white"></i>Deshabilitar o habilitar				 
				     </a>
                                </td>
                                		            
                              </tr>
                      </table>
                      <table class="table table-striped" >
                      
                               <tr>
				<td colspan="2">
                                  Elige esta opcion para modificar el cargo de las personas que laboran en el Punto Libre. Introduciendo el codigo de Punto especificado en la tabla:
				</td>
                                
                                  <td>
				  <a class="btn btn-primary btn-mini" href="#myModal3"  data-toggle="modal" role="button" >
				  <i class="icon-file icon-white"></i>Modificar el cargo				 
				     </a>
                                </td>
                                </tr>

                             </table>
                      
                            <div id="myModal1" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">Introduce el Codigo del Punto que desea modificar</p>
                                  </div>
                                   <div class="modal-body">
                                                Codigo:
                                   </div>
                                    <form method="post" action="modificar_punto.php" class="form-search">
                                           <div class="modal-footer">
                                             
                                                    <input  type="text" name="idpunto_libre" class="input-medium search-query">
                                                    <input type="submit"  class="btn btn-primary">
                                          </div>
                                   </form>
                              </div>
                                 <div id="myModal2" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">Introduce el Codigo del Punto que desea Deshabilitar</p>
                                  </div>
                                   <div class="modal-body">
                                                Codigo:
                                   </div>
                                    <form method="post" action="deshabilitacion.php" class="form-search">
                                           <div class="modal-footer">
                                             
                                                    <input  type="text" name="punto_libre_idpunto_libre" class="input-medium search-query">
                                                    <input type="submit"  class="btn btn-danger">
                                          </div>
                                   </form>
                              </div>
                                   <div id="myModal3" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">Introduce el Codigo del Punto que desea modificar el cargo</p>
                                  </div>
                                   <div class="modal-body">
                                                Codigo:
                                   </div>
                                    <form method="post" action="modificar_cargo.php" class="form-search">
                                           <div class="modal-footer">
                                             
                                                    <input  type="text" name="idpunto_libre" class="input-medium search-query">
                                                    <input type="submit"  class="btn btn-primary">
                                          </div>
                                   </form>
                              </div>
                      
                      
                      
                             <br>
                             <br>
                             <br>
                      
</div>

</div> 
</div>                              <script type="text/javascript" src="../js/jquery.js"></script>
                                   <script type="text/javascript" src="../js/bootstrap.js"></script>
                                 <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
                                  
                                 
                                

  
                                 
                            </script>
<!--
                            <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
                          -->
                          <script type="text/javascript">
                              $('#myModal1').on('hidden', function ()
                                                  $('#myModal2').on('hidden', function ()
                                                     $('#myModal3').on('hidden', function ()
                                                {
                              // do something…
                              })</script>
                            
</div>
</body>

</html>

<?php

}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permiso para ingresar a este modulo del sistema.');
                      document.location=('../html/paginaprincipal.php');
                  </script>";
		  
  header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
?>
