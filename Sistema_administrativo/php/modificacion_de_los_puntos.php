
<!DOCTYPE html>


<html lang="es">
  <head>
    <meta charset="utf-8" />
    <title>Modificacion de los puntos libres</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <meta name="description" content="">
    <link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/>   
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css" media="all" />
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">

  </head>

  <body>
    <div class="navbar navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
           <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
           </button>
          <a class="brand" href="#"><img src="../img/punto-small.png"/> Punto Libre</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li ><a href="menu_definitivo.html"><i class="icon-home"></i>Inicio</a></li>
              <li class=" dropdown active"><a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home"></i> Administración de P.L.<b class="caret"></b></a>
                <ul class="dropdown-menu">
                     <li><a href="permisologia.HTML"> <i class="icon-lock"></i>Permisología</a></li>
                         <li><a href="consulta_de_usuarios.HTML"><i class="icon-user"></i> Consulta de usuarios</a></li>
                          <li><a href="consulta_de_usuarios.HTML"><i class=" icon-time"></i> Historial de usuario</a></li>
                         <li><a href="#"> <i class="icon-wrench"></i> Respaldar base de datos</a></li>
                          <li><a href="reporte.HTML"><i class="icon-file"></i> Reportes</a></li>
                          <li><a href="#"><i class="icon-search"></i> </i> Consulta general</a></li>
                          <li><a href="menu_crear_punto.HTML"><i class="icon-edit"></i> Crear Punto</a></li>
                           <li><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i> Ubicación de los Puntos</a></li>
                           <li><a href="modificacion_de_los_puntos.php"><i class="icon-check"></i> Modificar y Deshabilitar los Puntos Libres</a></li>
                         
                        
                          <li><a href="ayuda.html"><i class="icon-question-sign"></i> Ayuda</a></li>
                    </li>
                 </ul>

              </li>
                    <li><a href="#"><i class="icon-book"></i> Gestión Cursos</a></li>
                    <li><a href="#"><i class="icon-tasks"></i> Organización Inventario</a></li>
                    <li><a href="#"><i class="icon-hdd"></i> Soporte a Equipos</a>
                    </li>
                  </ul>
                  <div class="pull-right">
                    <ul class="nav pull-right">
                      <li  class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> Usuario <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="perfil.html"><i class="icon-user"></i> Perfil</a></li>
                          <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda General</a></li>
                        </ul>
                            <li> 
                              <a href="#myModal" data-toggle="modal"> <i class="icon-off icon-white"></i>Cerrar sesión </a>
                              <div id="myModal" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">¿Seguro que quieres salir del Sistema?.</p>
                                  </div>
                                   <div class="modal-body">
                                                Pulsa Salir o Cancelar
                                   </div>
                                   <div class="modal-footer">
                                       <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a><a href="paginaprincipal.HTML" class="btn btn-primary">Salir</a>
                                  </div>
                            </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>                                                
            <br>
            <br>
            <div class="container-fluid">
                  <div class="row-fluid">
                    <div class="span3">
                        <div class="well sidebar-nav">
                          <ul class="nav nav-pills nav-stacked">
                            <li class="nav-hearder "><h4>Administración General</h4></li>
                            <li title="Iras a la pantalla principal del sistema"><a href="menu_definitivo.html"><i class="icon-home"></i>Inicio</a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="permisologia.HTML" ><i class="icon-lock"></i> Permisologia </a></li>
                            <li title="Solo pueden acceder los Administradores"><a href="menu_crear_punto.HTML"><i class="icon-pencil"></i>Crear Punto</a></li>
                            <li title="Veras la ubicación de todos los puntos"><a href="ubicacion_de_los_puntos.php"><i class="icon-globe"></i>Ubicación de los Puntos</a></li>
                            <li title="Reportes de todos los inconvenientes en el sistema"><a href="reporte.HTML"><i class="icon-file"></i> Reportes</a></li>
                            <li class="active" title="Modificacion y deshabilitacion de los Puntos"><a href="modificacion_de_los_puntos.HTML"><i class="icon-check"></i>  Modificar y deshabilitar P.L</a></li>
      
                        
                            <li class="dropdown ">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-wrench"></i> Herramientas <b class="caret"></b></a> 
                              <ul class="dropdown-menu ">
                                <li class="disabled"><a href="#">Respaldar la Base de Datos </a></li>
                                <li class="disabled"><a href="#">Restaurar la Base de Datos </a></li>
                                <li class="divider"></li>
                                 <li><a href="ayuda.html"><i class="icon-headphones"></i> Ayuda</a></li>
                              </ul> 
                            </li>
                          </ul>
                        </div>
                    </div>


                        <div class="span9">
                        <div class="hero-unit">
                          <h3 class="text-center">Modificacion de los puntos libres</h3>
                    		  <table id="tabla">
                    
                    							  
                    						  <div class="row-fluid">
                    						  <div class="span12 text-center btn-primary">
                    						<span>Estados donde se encuentran los Punto libre</span></div>
                    						</div>
                                    
                       
                      
                     <table class="table table-striped"align="center" border="6" bordercolor="#0080C0">

                    				   <tr>
                                  
                                    <td colspan="20"><h5 align="center">Elije el estado y Dirección donde deseas modificar o Deshabilitar</h5></align> </td>
                                    </tr>
                            
                                    
                                       <?php
                          include("conexion_administracion.php");

                                 $result=mysql_query("SELECT * FROM punto_libre");
                                        echo "<table  class=table table-striped border=4 bordercolor=#0080FF cellpadding=2 align=center>";
  
                                     echo"<tr><th>Estado</th><th>Direccion</th><th><h5>Codigo de Punto</h5></th><th colspan=2><h5>Deshabilitar</h5></th></tr>";
                                      while($reg=mysql_fetch_array($result))
                                           {
    
                                       echo "<tr><td>";
                                        echo $reg["estado"]. "</td><td>";
    
                                         echo $reg["direccion"]. "</td><td>";

                                          echo $reg["codigoP"]. "</td>";

                                          echo "<th colspan=2><input type=checkbox name=opcion4 value=Adicionar></th> ";




    
                                        }
                            
  
                                   mysql_close($mycon);
  
                                          ?>
    
                                   <tr>
                                
                                 
                                         <td colspan="2">Elige esta opcion para modificar el Punto Libre que desees. Introduciendo el codigo de Punto especificado en la tabla:</td> 
                                         <td><a class="btn btn-primary btn-small" href="#myModal1" data-toggle="modal" role="button"><i class="icon-file icon-white"></i> Modificar</a>
                                                                                            </td>

                                                    
                        

                  
                                     <th>
                                           <a class="btn btn-small btn-danger" href="deshabilitacion.HTML"><i class="icon-user icon-remove icon-white"></i></a></td>
    
                                      </th>           
                                  </tr>
                                  <div id="myModal1" class="modal hide fade">
                                  <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x</button>
                                      <p class="info">Introduce el Codigo del Punto que desea modificar</p>
                                  </div>
                                   <div class="modal-body">
                                                Codigo:
                                   </div>
                                    <form method="post" action="modificar_punto.php" class="form-search">
                                           <div class="modal-footer">
                                             
                                                    <input  type="text" name="codigoP" class="input-medium search-query">
                                                    <input type="submit" name="buscar" class="btn btn-primary">
                                          </div>
                                   </form>
                    
                                    
							              
         
                             </table>
                             <br>
                             <br>
                             <br>
  
           </div>
        </div>

      </div> 
  </div>
         <script type="text/javascript" src="../js/bootstrap.js"></script>
        <script type="text/javascript" src="../js/jquery.js"></script>
       <script type="text/javascript" src="../js/bootstrap-dropdown.js"> </script>
        
       
      

<script type="text/javascript">
${"dropdown-toggle"}.dropdown{}
       
  </script>

  <script type="text/javascript" src="../js/bootstrap-modal.js"> </script>
<script type="text/javascript">
    $('#myModal').on('hidden', function () {
    // do something…
    })</script>

</body>
</html>



