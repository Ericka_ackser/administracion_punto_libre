<?php

/*  **********************************************************************************************************************
    FUNCIÓN PARA CONVERTIR UN ARRAY ASOCIATIVO EN UNA CADENA EN FORMATO XML.
    PARÁMETROS:
    -----------
    $data           --> ES EL ARRAY DE ORIGEN
    $rootNodeName   --> INDICA CÓMO SE LLAMARÁ EL NODO RAÍZ.
    $xml            --> NO CAMBIAR. INDICA EL NODO DÓNDE ESTA. SE USA PARA EL PROCESO RECURSIVO.
    **********************************************************************************************************************
/**
 * Function for convert an array associative to XML string.
 * @param string $data The source array.
 * @param string $rootNodeName Indicates how to call at root node.
 * @param string $xml Is an attributte that is necessary for operation of the method.
 * @return string Returns a XML string.
 */ 
function array2XML($data, $rootNodeName = 'results', $xml=NULL){
    if ($xml == null){
        $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
    }

    foreach($data as $key => $value){
        if (is_numeric($key)){
            $key = "nodeId_". (string) $key;
        }

        if (is_array($value)){
            $node = $xml->addChild($key);
            array2XML($value, $rootNodeName, $node);
        } else {
            $value = htmlentities($value);
            $xml->addChild($key, $value);
        }

    }

    return html_entity_decode($xml->asXML());
}
?>
