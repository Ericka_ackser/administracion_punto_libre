<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();
?>
	<div class="span9">
	  <div class="hero-unit">
	    <h3 class="text-center">Equipos de Computación</h3>
	    <form method="post" action="insertar_periferico.php">
	      <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
	      <span>Datos del Bien</span>
	    </div>
	  </div><br />
	  <div class="row-fluid">
	    <div class="span3">
		Código del Bien:
	    </div>
	    <div class="span3">
	      <input type="text" name="cod_bien" required placeholder="#######" title="Introducir Código del Bien" size="20"/>
	    </div>
	    <div class="span3">
	      Fecha de Adquisición:
	    </div>
	     <div class="control-group">
	     <div class="controls input-append date form_date" data-date="" data-date-format="dd mm yyyy" 
		   data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
		  <input size="16" type="text" name="fecha_adq" value="" readonly>
		  <span class="add-on"><i class="icon icon-remove"></i></span>
		  <span class="add-on"><i class="icon icon-calendar"></i></span>
	     </div>
	     <input type="hidden" id="dtp_input2" value="" /><br/>
	</div>
	  </div>
	  <div class="row-fluid">
	     <div class="span3">
		Descripción del Bien:
	     </div>
	     <div class="span8">
		<textarea name="descripcion" required placeholder="Indique la descripción del bien para realizar el registro " rows="5" cols="75"></textarea>
	     </div>
	  </div>
	   <div class="row-fluid">
	     <div class="span3">
	        Forma de Adquisición:
	     </div>
	     <div class="span3">
	     Donación <input type="radio" name="forma_adq" value="Donacion" onclick="precio.disabled=true"/>
	     Compra <input type="radio" name="forma_adq" value="Compra" onclick=" precio.disabled=false"/>
	      </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		Proveedor:
	      </div>
	      <div class="span3">
		<input type="text" name="proveedor" size="20"/>
	      </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		  Precio (IVA Incluido):
	      </div>
	      <div class="span3">
	        <input type="text" name="precio" size="20"/>
	      </div>
	  </div><br>
	      <div class="row-fluid text-center btn-primary">
		<span>Componentes</span>
	      </div><br>
	      <div class="row-fluid text-center">
		<span>Componente Interno</span> <input type="radio" name="equipo" value="componente"/>
		<span>Periférico</span> <input type="radio" name="equipo" value="periferico"/>
	      </div>
	     <hr>
	      <div class="row-fluid">
		<div class="span4">
		   Marca: <input type="text" name="marca" size="20"/>
		</div>
		<div class="span4">
		   Modelo: <input type="text" name="modelo" size="20"/>
		</div>
		<div class="span4">
		   Serial: <input type="text" name="serial" size="20"/>
		</div>
	      </div><br>
		<hr>
	      <div class="row-fluid text-center">
		<div class="span12">
			Observación:<textarea name="observacion"  required placeholder="Indique el tipo de obsevacion que desea registrar del bien a incorporar " cols="75" rows="5"></textarea>
		</div>
	      </div>
	      <hr><br />
	      <div class="row-fluid">
		<div class="span12 text-center">
		  <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i>Guardar</button> 
		  <a class="btn btn-primary" href="#myModal" data-toggle="modal" role="button"><i class="icon-file icon-white"></i>Limpiar</a>
		  <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i>Cancelar</button>
		</div>
	      </div>
	    </form>
	  </div>
	</div>
      </div>
    </div>
    <?php
require('piepagina.php');
?>
