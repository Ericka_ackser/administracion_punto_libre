<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();
?>
	<div class="span9">
	  <div class="hero-unit">
	    <h3 class="text-center">Consulta de Bien</h3>
	    <div class="row-fluid">
	      <div class="span12 text-center btn-primary">
		<span>Consulta por Estatus</span>
	      </div>
	    </div><br />
	    <div class="row-fluid text-center" title="Elije el tipo de bien que desea consultar por estatus">
	      <div class="btn-group">
		<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Equipos Averiados<span class="caret"></span></button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="consultarpc-averiado.php">PC</a></li>
		  <li><a href="consultarcomponente-averiado.php">Componente Interno</a></li>
		  <li><a href="consultarperiferico-averiado.php">Periférico</a></li>
		</ul>
	      </div><!-- /btn-group -->
	      <div class="btn-group">
		<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Bienes Desincorporados<span class="caret"></span></button>
		<ul class="dropdown-menu" role="menu">
		  <li><a href="consultarpc-desincorporado.php">PC</a></li>
		  <li><a href="consultarcomponente-desincorporado.php">Componente Interno</a></li>
		  <li><a href="consultarperiferico-desincorporado.php">Periférico</a></li>
		  <li><a href="consultarmobiliario-desincorporado.php">Mobiliario</a></li>
		  <li><a href="consultarinsumooficina-desincorporado.php">Insumo de Oficina</a></li>
		 <li><a href="consultarinsumolimpieza-desincorporado.php">Insumo de Limpieza</a></li>
		</ul>
	      </div><br><br><hr>
	    </div>
	  </div>
	</div>
      </div>
    </div>
     <?php
  require('piepagina.php');
  ?>