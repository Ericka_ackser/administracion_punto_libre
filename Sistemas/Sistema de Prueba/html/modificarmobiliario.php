<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();

$codigo = $_GET['cod_bien'];
$sql = "SELECT * FROM recepcion,bien,mobiliario WHERE bien.cod_bien = mobiliario.cod_bien and bien.cod_bien = '$codigo' LIMIT 0,1";

$result = mysql_query($sql);
$row = mysql_fetch_assoc($result);
?>
  <div class="span9">
      <div class="hero-unit">
	<h3 class="text-center">Modificar Mobiliario</h3>
	<form method="post" action="actualizarmobiliario.php">
	  <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
		<span>Registro</span>
	    </div>
	  </div><br />
	  <div class="row-fluid">
	    <div class="span3">
	      No de Registro:
	    </div>
	    <div class="span3">
	      <input type="text" name="cod_registro" size="20" readonly="readonly" value="<?=$row['cod_registro']?>"/>
	    </div>
	    <div class="span3">
	      Fecha de Registro:
	    </div>
	    <div class="span3">
	      <input type="text" name="fecha_registro" size="20" value="<?=$row['fecha_registro']?>"/>
	    </div>
	  </div><br>
	  <div class="row-fluid">
	    <div class="span1">
		  Estado:
	    </div>
	    <div class="span2">
	      <select name="idpuntolibre">
		<option value="">Seleccione...</option>
		  <?PHP
		    $busq_m=mysql_query("select * from inventario.punto_libre ORDER BY idpuntolibre");
		      while($reg_m=mysql_fetch_array($busq_m))
		      {
		      echo "<option value='".$reg_m['idpuntolibre']."' >".$reg_m['estado']."</option>";
		      }			
		   ?>
	      </select>
	    </div>
	    <div class="span1"></div>
	    <div class="span1">
		Ciudad:
	    </div>
	    <div class="span2">
	      <select name="idpuntolibre">
		<option value="">Seleccione...</option>
		  <?PHP
		    $busq_m=mysql_query("select * from inventario.punto_libre ORDER BY idpuntolibre");
		      while($reg_m=mysql_fetch_array($busq_m))
		      {
		      echo "<option value='".$reg_m['idpuntolibre']."' >".$reg_m['ciudad']."</option>";
		      }			
		   ?>
	      </select>
	    </div>
	    <div class="span1"></div>
	    <div class="span2">
	      Ubicación:
	    </div>
	    <div class="span2">
	      <select name="idpuntolibre">
		<option value="">Seleccione...</option>
	      </select>
	    </div>
	  </div><hr>
	  <div class="row-fluid">
	    <div class="span3">
		Código del Bien:
	    </div>
	    <div class="span3">
		<input type="text" name="cod_bien" title="Introducir Código" size="20" readonly="readonly" value="<?=$row['cod_bien']?>"/>
	    </div>
	    <div class="span3">
		Fecha de Adquisición:
	    </div>
	    <div class="span3">
		<input type="text" name="fecha_adq" title="" size="20" value="<?=$row['fecha_adq']?>"/>
	    </div>
	  </div>
	  <div class="row-fluid">
	    <div class="span3">
	      Descripción del Bien:
	    </div>
	    <div class="span8">
	      <textarea name="descripcion" rows="5" cols="67"><?=$row['descripcion']?></textarea>
	    </div>
	  </div>
	  <div class="row-fluid">
	     <div class="span3">
	        Forma de Adquisición:
	     </div>
	     <div class="span3">
	     Donación <input type="radio" name="forma_adq" onclick="precio.disabled=true"
	     value="Donacion" <?php if ($row['forma_adq'] == "Donacion") { echo "checked=\"checked\"";} ?> onclick="selec()"/>
	     Compra <input type="radio" name="forma_adq" onclick=" precio.disabled=false"
	     value="Compra" <?php if ($row['forma_adq'] == "Compra") { echo "checked=\"checked\"";} ?> onclick="selec()"/>
	      </div>
	    <div class="span3">
		Proveedor:
	    </div>
	    <div class="span3">
	      <input type="text" name="proveedor" size="20" value="<?=$row['proveedor']?>"/>
	    </div>
	  </div>
	  <div class="row-fluid">
	    <div class="span3">
		Precio (IVA Incluido):
	    </div>
	    <div class="span3">
	      <input type="text" name="precio" size="20" value="<?=$row['precio']?>"/>
	    </div>
	  </div><br />
	  <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
		<span>Mobiliario</span>
	    </div>
	  </div><br />
	  <div class="row-fluid text-center">
	    <div class="span3"></div>
	    <div class="span2">
		    Marca: <input type="text" name="marca_mobiliario" size="10" value="<?=$row['marca_mobiliario']?>"/>
	    </div>
	    <div class="span2">
		  Modelo: <input type="text" name="modelo_mobiliario" size="10" value="<?=$row['modelo_mobiliario']?>"/>
	    </div>
	    <div class="span2">
		  Serial: <input type="text" name="serial_mobiliario" size="10" value="<?=$row['serial_mobiliario']?>"/>
	    </div>
	  </div><br>
	  <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
		<span>Características</span>
	    </div>
	  </div><br />
	  <div class="row-fluid text-center">
	    <div class="span3"></div>
	    <div class="span2">
		    Material: <input type="text" name="material_mobiliario" size="10" value="<?=$row['material_mobiliario']?>"/>
	    </div>
	    <div class="span2">
		  Color: <input type="text" name="color_mobiliario" size="10" value="<?=$row['color_mobiliario']?>"/>
	    </div>
	    <div class="span2">
		  Dimensión: <input type="text" name="dimension_mobiliario" size="10" value="<?=$row['dimension_mobiliario']?>"/>
	    </div>
	  </div><hr>
	  <div class="row-fluid">	
		<div class="span2">
		    <span>Estado</span>
		</div>
		<div class="span7">
		    <input type="radio" name="estado" value="1" <?php if ($row['estado'] == "1") { echo "checked=\"checked\"";} ?> onclick="selec()"/> Desincorporado
		</div>
	      </div><hr>
	  <div class="row-fluid text-center">
	    <div class="span12">
		Observaciones:<textarea name="observacion_mobiliario" cols="79" rows="4"><?=$row['observacion_mobiliario']?></textarea>
	    </div>
	  </div><hr><br />
	  <div class="row-fluid">
	    <div class="span12 text-center">
	  <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i>Actualizar</button> 
	  <button type="reset" class="btn btn-primary"><i class="icon-file icon-white"></i>Limpiar</button> 
	  <a href="descripcion_mobiliario.php?cod_bien=<?=$row['cod_bien']?>" class="btn btn-danger"><i class="icon-remove icon-white"></i>Cancelar</a>
	    <a href="ReporteMobiliario.php?id='.$codigo.'">ver</a></td>
	    </div>
	  </div>
	</form>
      </div>
    </div>
  </div>
</div>
<?php
  require('piepagina.php');
?>
