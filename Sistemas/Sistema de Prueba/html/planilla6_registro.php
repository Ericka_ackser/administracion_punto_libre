<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();
?>
   
	<div class="span9">
	  <div class="hero-unit">
	    <h3 class="text-center">Modificar Bien</h3>
	    <form method="post" action="#">
	      <div class="row-fluid">
		<div class="row-fluid">
		   <div class="span12 text-center btn-primary">
		       <span>Registro</span>
		   </div>
	        </div><br />
	        <div class="row-fluid">
		  <div class="span3">
		     N° de Registro:
		  </div>
		  <div class="span3">
		     <input type="text" value=" 001" name="codigo" size="20"/>
		  </div>
		  <div class="span3">
		     Fecha de Registro:
		  </div>
		  <div class="span3">
		     <input type="text" value="29/07/2013" name="codigo" size="20"/>
		  </div>
	        </div>
	        <div class="row-fluid">
		  <div class="span3">
		     Sede:
		  </div>
		  <div class="span3">
		     <input type="text"value="Merida" name="codigo" size="20"/>
		  </div>
		  <div class="span3">
		     Local:
		  </div>
		  <div class="span3">
		     <input type="text"value="Humbolt" name="codigo" size="20"/>
		  </div>
	        </div>
	        <div class="row-fluid">
		  <div class="span3">
		     Área:
		  </div>
		  <div class="span3">
		     <input type="text" value="Administrativa"name="codigo" size="20"/>
		  </div>
		  <div class="span3">
		     Oficina:
		  </div>
		  <div class="span3">
		     <input type="text" value="n° 1"name="codigo" size="20"/>
		  </div>
	        </div>
	      </div><br />	
	      <div class="row-fluid">
		<div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Datos del Bien</span>
		</div>
	      </div><br />
	      <div class="row-fluid">
		<div class="span3">
		  Código del Bien:
		</div>
		<div class="span3">
		  <input type="text" value="002"name="codigo" size="20"/>
		</div>
		<div class="span3">
		  Fecha de Adquisición:
		</div>
		<div class="span3">
		  <input type="text" value="17/06/2013"name="codigo" size="20"/>
		</div>
	      </div>
	      <div class="row-fluid">
		<div class="span3">
		  Descripción del Bien:
		</div>
		<div class="span8">
		  <textarea name="descripcion" rows="5" cols="74" placeholder="Disco duro de 160 gb Marca Samsung sata"></textarea>
		</div>
	      </div>
	      <div class="row-fluid">
		<div class="span3">
		  Forma de Adquisición:
		</div>
		<div class="span3">
		  <select>
		    <option value="value">Compra</option>
		    <option value="value">Alquiler</option>
		    <option value="saab">Donacion</option>
		  </select>
		</div>
		<div class="span3">
		  Proveedor:
		</div>
		<div class="span3">
		  <input type="text"value="Otiesca C.A" name="codigo" size="20"/>
		</div>
	      </div>
	      <div class="row-fluid">
		<div class="span3">
		  Cantidad:
		</div>
		<div class="span3">
		  <input type="text"value="1" name="codigo" size="20"/>
		</div>
		<div class="span3">
		  Precio (IVA Incluido):
		</div>
		<div class="span3">
		  <input type="text"value="2000 bs" name="codigo" size="20"/>
		</div>
	      </div><br />
	      <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Especificaciones</span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span3"></div>
		<div class="span2">
		      Marca: <input type="text" value="sansumg" name="codigo" size="10"/>
		</div>
		<div class="span2">
		      Modelo: <input type="text" value="sn1264" name="codigo" size="10"/>
		</div>
		<div class="span2">
		      Serial: <input type="text" value="mde4535" name="codigo" size="10"/>
		</div>
	      </div><br>
	      <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Características</span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span3"></div>
		<div class="span2">
			Material: <input type="text" value="metal"name="codigo" size="10"/>
		</div>
		<div class="span2">
		      Color: <input type="text" value="plateado"name="codigo" size="10"/>
		</div>
		<div class="span2">
		      Dimensión: <input type="text" value="lapto"name="codigo" size="10"/>
		</div>
	      </div><hr>
	      <div class="row-fluid">
		<div class="span12">
		  <div class="span3">
			Estado del Bien:	
		  </div>
		  <div class="span2">
			<input type="radio" name="codigo" value="nuevo"> Nuevo	
		  </div>
		  <div class="span2">
			<input type="radio" name="codigo" value="bueno"> Bueno	
		  </div>	
		  <div class="span2">
			<input type="radio" name="codigo" value="regular"> Regular	
		  </div>
		  <div class="span2">
			<input type="radio" name="codigo" value="malo"> Malo	
		  </div>
		</div>
	      </div><hr>
	      <div class="row-fluid text-center">
		<div class="span12">
			Observaciones:<textarea name="direccion" cols="100" rows="4"></textarea>
		</div>
	      </div>
	      <hr><br />
	    <div class="row-fluid">
		<div class="span12 text-center">
		  <a class="btn btn-primary" href="#myModal" data-toggle="modal" role="button"><i class="icon-file icon-white"></i>Guardar</a>
		  <button type="reset" class="btn btn-primary"><i class=" icon-file icon-white"></i>Limpiar</button>
		   <a class="btn btn-danger" href="#myModal" data-toggle="modal" role="button"><i class="icon-remove icon-white"></i>Cancelar</a>
		</div>
	      </div>
	     </div>
	    </form>
	  </div>
	</div>
      </div>
    </div>
    <?php
require('piepagina.php');
?>