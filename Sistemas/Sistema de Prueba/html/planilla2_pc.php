<?php
require('cabecera.php');
require('menu.php');
require('conexion.php');
conexion();
?>
	<div class="span9">
	  <div class="hero-unit">
	    <h3 class="text-center">Equipos de Computación</h3>
	    <form method="post" action="insertar_pc.php">
	      <div class="row-fluid">
	    <div class="span12 text-center btn-primary">
	      <span>Datos del Bien</span>
	    </div>
	  </div><br />
	  <div class="row-fluid">
	    <div class="span3">
		Código del Bien:
	    </div>
	    <div class="span3">
	      <input type="text" name="cod_bien" required placeholder="#######" title="Introducir Código del Bien" size="20"/>
	    </div>
	    <div class="span3">
	      Fecha de Adquisición:
	    </div>
	    <div class="control-group">
	     <div class="controls input-append date form_date" data-date="" data-date-format="dd mm yyyy" 
		   data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
		  <input size="16" type="text" name="fecha_adq" value="" readonly>
		  <span class="add-on"><i class="icon icon-remove"></i></span>
		  <span class="add-on"><i class="icon icon-calendar"></i></span>
	     </div>
	     <input type="hidden" id="dtp_input2" value="" /><br/>
	</div>
	  </div>
	  <div class="row-fluid">
	     <div class="span3">
		Descripción del Bien:
	     </div>
	     <div class="span8">
		<textarea name="descripcion" required placeholder="Indique la descripción del bien para realizar el registro " rows="5" cols="79"></textarea>
	     </div>
	  </div>
	  <div class="row-fluid">
	     <div class="span3">
	        Forma de Adquisición:
	     </div>
	     <div class="span3">
	     Donación <input type="radio" name="forma_adq" value="Donacion" onclick="precio.disabled=true"/>
	     Compra <input type="radio" name="forma_adq" value="Compra" onclick=" precio.disabled=false"/>
	      </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		Proveedor:
	      </div>
	      <div class="span3">
		<input type="text" name="proveedor" size="20"/>
	      </div>
	  </div>
	  <div class="row-fluid">
	      <div class="span3">
		  Precio (IVA Incluido):
	      </div>
	      <div class="span3">
	        <input type="text" name="precio" size="20"/>
	      </div>
	  </div><br>
	      <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Case</span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span2">
		  Marca:<input type="text" name="marca" size="10"/>
		</div>
		<div class="span2">
		  Serial:<input type="text" name="serial" size="10"/>
		</div>
		<div class="span2">
		  DD:<input type="text" name="dd" size="10"/>
		</div>
		<div class="span2">
		  RAM: <input type="text" name="ram" size="10"/>
		</div>
		<div class="span2">
		  Placa: <input type="text" name="placa" size="10"/>
		</div>
		<div class="span2">
		  CD / DVD
		    <select name="unidad" required>
			<option>Seleccione...</option>
			<option value="SI">SI</option>
			<option value="NO">NO</option>
		    </select>
		</div>
	      </div><br>
	     <div class="row-fluid">
		<div class="span6 text-center btn-primary">
			<span>Monitor</span>
		</div>
		<div class="span6 text-center btn-primary">
			<span>Procesador</span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span2">
		  Marca: <input type="text" name="marca_monitor" size="10"/>
		</div>
		<div class="span2">
		   Color: <input type="text" name="color_monitor" size="10"/>
		</div>
		<div class="span2">
		    Pulgada: <input type="text" name="pulgada" size="10"/>
		</div>
		<div class="span2">
		   Marca: <input type="text" name="marca_procesador" size="10"/>
		</div>
		<div class="span2">
		   Tipo: <input type="text" name="tipo" size="10"/>
		</div>
		<div class="span2">
		   GHZ: <input type="text" name="ghz" size="10"/>
		</div>
	      </div><br>
	      <div class="row-fluid">
		<div class="span6 text-center btn-primary">
			<span>Mouse</span>
		</div>
		<div class="span6 text-center btn-primary">
			<span>Teclado</span>
		</div>
	      </div><br />
	      <div class="row-fluid text-center">
		<div class="span1"></div>
		<div class="span2">
		   Color: <input type="text" name="color_mouse" size="10"/>
		</div>
		<div class="span2">
		  Tipo
		    <select name="tipo_mouse" required>
			<option>Seleccione...</option>
			<option value="Trackball">Trackball</option>
			<option value="Óptico">Óptico</option>
		    </select>
		</div>
		<div class="span1"></div>
		<div class="span2">
		   Color: <input type="text" name="color_teclado" size="10"/>
		</div>
		<div class="span2">
		   Marca: <input type="text" name="marca_teclado" size="10"/>
		</div>
		<div class="span2">
		   Serial: <input type="text" name="serial_teclado" size="10"/>
		</div>
	      </div><br><hr>
	      <div class="row-fluid text-center">
		<div class="span12">
		  Observación:<textarea name="observacion"  required placeholder="Indique el tipo de obsevacion que desea registrar del bien a incorporar " cols="79" rows="5"></textarea>
		</div>
	      </div>
	      <hr><br />
	      <div class="row-fluid">
		<div class="span12 text-center">
		  <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i>Guardar</button> 
		  <button type="reset" class="btn btn-primary"><i class="icon-file icon-white"></i>Limpiar</button>
		  <a href="planilla2.php" class="btn btn-danger"><i class="icon-file icon-white"></i>Atras</a>
		</div>
	      </div>
	    </form>
	  </div>
	</div>
      </div>
    </div>
    <?php
require('piepagina.php');
?>