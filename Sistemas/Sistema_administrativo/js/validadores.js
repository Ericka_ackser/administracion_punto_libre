
function validar_cedula(cedula) {
    
    
    var expreg=new RegExp("^(V|E)-[1-9]+[0-9]{3,8}$");
    if (expreg.test(cedula)) {
        return true;
    
    }
    return false;
}
var validarFecha = function(fecha){
 //Funcion validarFecha 
 //Escrita por Buzu feb 18 2010. (FELIZ CUMPLE BUZU!!!
 //valida fecha en formato aaaa-mm-dd
 var fechaArr = fecha.split('-');
 var aho = fechaArr[0];
 var mes = fechaArr[1];
 var dia = fechaArr[2];
 
 var plantilla = new Date(aho, mes - 1, dia);//mes empieza de cero Enero = 0

 if(!plantilla || plantilla.getFullYear() == aho && plantilla.getMonth() == mes -1 && plantilla.getDate() == dia){
 return true;
 }else{
 return false;
 }
}

/*	
function validarFormatoFecha(fecha_evento) {
      var RegExPattern =("/^\d{1,2}\/\d{1,2}\/\d{2,4}$/");
      if ((fecha_evento.match(RegExPattern)) && (fecha_evento!='')) {
            return true;
      } else{
            return false;
      }
      
}
*/
function validar_archivo(obj){
	obj=document.getElementById("imagenes")
	if(obj.value == ""){
		alert("Campo Obligatorio imagenes");
		obj.focus();
		return false;
		
		}
	
	}


function validar_telefono(telefono) {
    
    var expreg=new RegExp("^0(2|4)((14|16|24|12|26)|[0-9]{2})-[0-9]{7}$"); 
    
    if (expreg.test(telefono)) {
        return true;
        
    }
    return false;
}

function campos_iguales(ida,idb, campo) {
    if (document.getElementById(ida).value==document.getElementById(idb).value) {
        return true;
    }
    alert("Los campos "+ campo +"no coinciden");
    

    return false;
   
}

function validar_formulario(formulario){
    
    for (var i=0; i<formulario.elements.length; i++) {
    
        if (formulario.elements[i].id=="cedula") {
            res=validar_cedula(formulario.elements[i].value)
            if (res==false) {
                alert("El campo "+ formulario.elements[i].id + " no cumple la estrucutra VE-XXXXXXX" );
                formulario.elements[i].focus();
                return;
            }
        }
        
        if (formulario.elements[i].id=="telefono") {
            res=validar_telefono(formulario.elements[i].value)
            if (res==false) {
                alert("El campo "+ formulario.elements[i].id + " no cumple la estrucutra XXXX-XXXXXXX" )
                formulario.elements[i].focus();
                return;
                
                }
        }

           if (formulario.elements[i].id=="fecha") {
            res=validarFecha(formulario.elements[i].value)
            if (res==false) {
                alert("El campo "+ formulario.elements[i].id + " no cumple la estrucutra XXXX-XXXXXXX" )
                formulario.elements[i].focus();
                return;
                
                }
        }

        
        
    
        
    }
    
    
    if(campos_iguales("pass1", "pass2", "claves")==false)
    
    {
        return true;
        
    }
    
    
    document.forms[0].submit();
    return true;
}

function obtener_objeto(objeto_id){
    
    return document.getElementById(objeto_id);
}
