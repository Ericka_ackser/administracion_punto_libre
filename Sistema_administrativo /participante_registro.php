<?php
     require('cabecera.php');
     require('menu.php');
     $con= @mysql_connect("localhost","root","21183652");
     
     if(!mysql_select_db("administracion", $con)){
         echo "error al conectarse";}
         include("../../Sistema_administrativo/php/sesion.php");
if(isset($_SESSION) and array_key_exists("login",$_SESSION) and $_SESSION['login']==true
and $_SESSION['Acceso_Cursos']==1 and $_SESSION['Insertar_Informacion']==1){
        
     if(!mysql_select_db("sme", $con)){
         echo "error al conectarse";}     
     
?>
<div class="span9"><!--Contenido-->
     <div class="hero-unit"><!--Bloque de Contenido Gris-->
	<h3 class="text-center">Registro del Participante</h3>
	<form method="post" action="participante_insertar.php">
	    <div class="row-fluid">
		<div class="span12 text-center btn-primary">
		    <span>Datos del Participante</span>
		</div>
	    </div>
	    <div class="row-fluid">
		<div class="span12"><br />
		    <div class="row-fluid">
			<div class="span2"><span>Cédula:</span></div>
			<div class="span7">
			    <input name="ci_part" class="input-block-level" type="text" placeholder="00.000.000" title="Solo debe introducir Números" pattern="[0-9]{0,}[.][0-9]{3}[.][0-9]{3}" required>
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Nombres:</span></div>
			<div class="span7">
			    <input name="nombre_part" class="input-block-level" type="text" size="40" maxlength="39" placeholder="Nombres del Participante" title="Solo debe introducir letras" pattern="[A-Z\a-z]{3,}[ ][A-Z\a-z]{3,}" required>
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Apellidos:</span></div>
			<div class="span7">
			    <input name="apellido_part" class="input-block-level" type="text" size="40" maxlength="39" placeholder="Apellidos del Participante" title="Solo debe introducir letras" pattern="[a-z\A-Z]{3,}[ ][a-z\A-Z]{3,}" required>
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Género:</span></div>
			<div class="span7" title="Elija el Genero del Participante">
			    <input  type="radio" name="genero_part" value="Masculino" checked="checked"/> Masculino
			    <input  type="radio" name="genero_part" value="Femenino" /> Femenino
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Dirección:</span></div>
			<div class="span7">					    
			    <textarea name="direccion_part" class="input-block-level" cols="64" rows="5" placeholder="Dirección del Participante" title="Puede introducir letras, Simbolos y Números" required></textarea>
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Teléfono:</span></div>
			<div class="span7">					    
			    <input name="tlfn_part" class="input-block-level" type="text" size="15" maxlength="12" placeholder="0000-0000000" title="Solo debe introducir números" pattern="[0-9]{4}[-][0-9]{7}" required>
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Correo:</span></div>
			<div class="span7">
			    <input name="correo_part" class="input-block-level" type="email" size="40" maxlength="30" placeholder="ejemplo@mail.com" title="Introduzca el correo del Participante">
			</div>
		    </div>
		    <div class="row-fluid">
			<div class="span2"><span>Ocupación:</span></div>
			<div class="span7">
			    <select name="ocup_part" class="input-block-level" title='Seleccione la ocupación del participante' requerid>
				<option>Seleccione...</option>
				<option value="Estudiante">Estudiante</option>
				<option value="Profesional">Profesional</option>
			    </select>
			</div>
		    </div>
		</div>
	    </div>
	    <hr><br />
	    <div class="row-fluid">
	       <div class="span12 text-center">
		    <button type="submit" class="btn btn-primary"><i class="icon-file icon-white"></i>Guardar</button>
		    <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i>Cancelar</button>
	       </div>
	    </div>
	</form><!--cierre del formulario-->
     </div><!--cierre del Hero-Unit-->
</div><!--cierre del contenido-->
</div><!--cierre del row-fluid de contenido-->
</div><!--cierre del container-->

<!------------------------ Modal ------------------------->
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header ">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Advertencia de Seguridad</h3>
</div>
<div class="modal-body alert alert-block">
<strong><p>¿Esta totalmente seguro que desea guardar los datos introducidos anterioremente?</p></strong>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
<a class="btn btn-primary" href="#myModal2" role="button" data-toggle="modal"><i class="icon-file icon-white"></i>Guardar</a>
</div>
</div>
<!-- Modal 2-->
<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="myModalLabel">Se ha guardado con Exito!</h3>
</div>
<div class="modal-body alert-success">
<p>¡En hora buena! Se han guardado exitosamente.</p>
<div class="progress progress-success progress-striped">
    <div class="bar" style="width: 100%"></div>
</div>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
<button type="submit" class="btn btn-primary"><i class="icon-print icon-white"></i>Volver</button>
</div>
</div>

<!------------------------ Modal ------------------------->

<?php


}else{
 echo "<script type=text/javascript>
                      alert(' No tiene permisos para consultar los cursos.');
                      document.location=('../../Sistema_administrativo/html/paginaprincipal.php');
                  </script>";
		  
  //header("Location:".$_CONF['server_web'].$_CONF['app']."html/paginaprincipal.php");
  
}
require('piepagina.php');
?>