<?php
session_start();
include("../conf.php");?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8" />
	<title>Registro de usuario</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" >
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rel="shortcut icon" type="image/x-icon" href="../img/punto-small.png"/> 
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">

<link rel="stylesheet" type="text/css" href="../css/main.css">
<script type="text/javascript" src="../js/validadores.js"></script>


</head>
 <body>
       
       
       
       <form  action="<?php echo $_CONF['server_web'].$_CONF['app']."php/usuario.php"; ?>" method="post" id="registro"> 

	<div class="hero-unit color_fondo" background :"#F0F8FF";>
<img src="../img/imagen1.png" alt="primera imagen"  class="img-rounded" align=left      width=100 height=100 >
<img src="../img/imagen2.png" alt="segunda imagen"  class="img-rounded"
         align=right width=90 height=90>
		<h2> Punto Libre GNU LINUX de Venezuela</h2>
		<p>Organizacion encargada de impulsar el software libre en Venezuela</p>
		
	


     </div>

	<h2 align=center >Registro de Usuario</h2>
	<?php if(array_key_exists("error_msg",$_SESSION)){ ?>
	    <div class="alert">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
	    <strong>Advertencia!</strong> <?php
	    
	    echo $_SESSION['error_msg'];
	    session_destroy();
	    ?>
	    </div>
	<?php } ?>
	<table align="center">
	 
		    <tr>
		    <td><p>Cédula de Identidad:</p></td> 
	     
		    <td><input type="text" id="cedula" name="cedula" value=""  size="22" placeholder="00.000.000" title="Solo debe introducir Números" pattern="[0-9]{0,}[.][0-9]{3}[.][0-9]{3}"  required></td>
		    
			
			</tr>
		    <tr>
			<td><p>Nombres:</p></td> 
			<td><input type="text" id="nombre" name="nombre" required="required" placeholder="Primer y segundo nombre del Usuario" size="22" title="Solo debe contener letras" pattern="[A-Z\a-z]{3,}[ ][A-Z\a-z]{3,}"></td> 
			</tr>
		    
			<tr>
			<td><p>Apellidos:</p></td>
		    <td><input type="text" id="apellido" name="apellido" value="" size="22" title="Solo debe contener letras"  placeholder="Primer y segundo Apellido del Usuario" title="Solo debe introducir letras" pattern="[a-z\A-Z]{3,}[ ][a-z\A-Z]{3,}"  required></td>
		    </tr>
			
		    <tr>
		    <td><p>Nombre de Usuario:</p></td> 
		    <td><input type="text" id="usuario" name="usuario" value="" size="22" title="Solo debe contener caracteres" required></td>
		    </tr>
		    <tr> 
		    <td><p>Clave:</p></td>
		    
		    <td><input type="password" id="pass1" name="pass1" value=""  size="22" title="Debe contener 7 caracteres como maximo"  required/></td>
		    </tr>
		    <tr>
		    <td><p>Repita la Clave:</p></td>
		    <td><input type="password" id="pass2" name="pass2" value="" placeholder="Repetir clave"  size="22" oninput="check1(this)" required></td>
		      <script>
		    
		    function check1(input) {
		    
		    if (input.value != document.getElementById('pass1').value) {
		    
		    input.setCustomValidity('Tienes que repetir la misma clave.');
		    
		    } else{
		    
		    // input is valid -- reset the error message
		    
		    input.setCustomValidity('');
		    
		    }
		    
		    }
		    
		    </script>    
		    </tr>
		    <tr>
		    <td><p>Sexo:</p></td>
		    <td><input type="radio" id="Sexo1" name="Sexo" value="F" size="22"/> Femenino</td>
		    <td><input type="radio" id="Sexo2" name="Sexo" value="M" size="22" /> Masculino</td>
		    </tr>
		    <tr>
		    <td><p>Correo electronico:</p>
	            <td><input type="email" id="email_addr" name="email1" value=""placeholder="Ejemplo@gmail.com" size="22" required></td>
	            </tr>
	        <tr>
		  <td>Repita el Correo electronico:</td>
		  <td><input type="email" id="email_addr_repeat" name="email" value="" placeholder="Ejemplo@gmail.com" size="22" oninput="check(this)" required/></td>
		  </tr>
		      <script>
		    
		    function check(input) {
		    
		    if (input.value != document.getElementById('email_addr').value) {
		    
		    input.setCustomValidity('Tienes que repetir el mismo correo.');
		    
		    } else{
		    
		    // input is valid -- reset the error message
		    
		    input.setCustomValidity('');
		    
		    }
		    
		    }
		    
		    </script>
		  <tr>
		  <td>Numero Telefonico:</td>
		  <td><input type="text" id="telefono" name="telefono" value="" placeholder="solo numeros 0000-0000000" size="22" title="Solo numeros" pattern="[0-9]{4}[-][0-9]{7}" required></td>
		  
      
	  
				</tr>
				<tr>
				<td><p>Estado de vivienda:</p></td> 
				<td colspan="6"><select name="estado" required >
					<option value="Estados"> Estados</option>
					<option value="Amazonas">Amazonas</option>
					<option value="Anzoátegui">Anzoátegui</option>
					<option value="Apure">Apure</option>
					<option value="Aragua">Aragua</option>
					<option value="Barinas">Barinas</option>
					<option value="Bolívar">Bolívar</option>
					<option value="Carabobo">Carabobo</option>
					<option value="Cojedes">Cojedes</option>
					<option value="Delta Amacuro">Delta Amacuro</option>
					<option value="Distrito Capital">Distrito Capital</option>
					<option value="Falcón">Falcón</option>
					<option value="Guárico">Guárico</option>
					<option value="Lara">Lara</option>
					<option value="Mérida">Mérida</option>
					<option value="Miranda">Miranda</option>
					<option value="Monagas">Monagas</option>
					<option value="Nueva Esparta">Nueva Esparta</option>
					<option value="Portuguesa">Portuguesa</option>
					<option value="Sucre">Sucre</option>
					<option value="Tachira">Tachira</option>
					<option value="Trujillo">Trujillo</option>
					<option value="Vargas">Vargas</option>
					<option value="Yaracuy">Yaracuy</option>
					<option value="Zulia">Zulia</option>
					</select>
					</td>
					</tr>
					<tr>
					<td><p>Direccion donde se encuentra residenciado:</p></td>
						<td><textarea title="Solo caracteres" id="Direccion" name="direccion"required name="Direccion" value=""  cols="25" rows="5"></textarea></td>
						</tr>
					<tr>
			<td><p>Cargo que cumple en el Punto libre:</p>


				
				<td colspan="6"><select name="cargo" required="required">
				<option value="cargo"> Cargo</option>
					<option value="Administrador"> Administrador</option>
					<option value="Encargado">Encargado Nacional</option>
					<option value="Regional">Encargado Regional</option>
					<option value="Punto">Encargado del Punto Libre</option>
					<option value="Secretaria">Secretaria</option>
					<option value="Ayudante">Ayudante</option>
					<option value="Docente">Docente</option></select></td>
                                        </tr>
								


					
	</table>
<br/><br/>

<div class="row-fluid">
                <div class="span12 text-center">
                  <div class="btn-group">
                  <button type="reset" class="btn btn-danger"><i class="icon-remove icon-white"></i> Limpiar</button>
                   <button class="btn btn-primary" value="Enviar" type="submit" > 
			<i class=" icon-pencil icon-white"></i> Enviar
                   </button>
                   </form>
                   <br>
                   <br>
                   	<form  action="paginaprincipal.php">
                   <button class="btn btn-danger" type="submit1" href="paginaprincipal.php"><i class=" icon-share-alt icon-white"></i> Regresar</button>
                   </form>
        
                   </div>
                </div>
              </div>

              
</body>
</html>
